<?php
namespace app\controllers;

use Yii;
use yii\rest\Controller;

class ApiController extends Controller {
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return ["hello world"];
    }
}