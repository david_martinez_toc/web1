<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use app\models\SmartIdForm;
use app\components\Helpers;
use yii\helpers\Json;

class SiteController extends Controller
{

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','smart-id'],
                'rules' => [
                    [
                        'actions' => ['logout','smart-id'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays SmartId.
     *
     * @return string
     */
    public function actionSmartId()
    {
        $model = new SmartIdForm();
        $model->name = true;
        $model->rut = true;
        $model->callback_url = Yii::$app->params['callback_url'];
        $qr_image = null;
        $qr_url = null;
        $response = null;
        if ($model->load(Yii::$app->request->post())) {
            $headers = [];
            $headers2 = [];
            $headers['Authorization'] = 'Bearer ' . Yii::$app->params['api_key'];
            //$headers['Accept'] = 'application/json';
            $headers['Content-Type'] = 'application/json';
            foreach ($headers as $k => $v) {
                $headers2[] = $k . ': ' . $v;
            }
            $filters = [];
            foreach($model->attributes as $attribute=> $value){
                if($value == 1 || $value == '1'){
                    $filters[$attribute] = true;
                }
            }
            $payload = Json::encode([
                'callbackUrl'=>Yii::$app->params['callback_url'],
                'filters'=> $filters,
            ]);
            try {
                Yii::info($headers2);
                Yii::info($payload);
                $response = Helpers::curl_get_contents(Yii::$app->params['endpoint'], $headers2, 'POST', $payload);
                Yii::info($response);
                if(isset($response->data)){
                    $qr_image = $response->data->qr_code;
                    $qr_url = $response->data->qr_content;
                }
            } catch (yii\base\Exception $e) {
                $response = $e->getTraceAsString();
            }

            Yii::$app->session->setFlash('smartIdFormSubmitted');
        }
        return $this->render('smart_id',['model'=>$model, 'qr_image'=>$qr_image, 'qr_url'=>$qr_url, 'response' => $response]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
