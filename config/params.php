<?php

return [
    'adminEmail' => 'admin@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'endpoint' => 'https://id-backend-sandbox.7oc.cl/client/generate_qr',
    'api_key' => 'eyJpdiI6IjR2dEZHV1ozSGwyalg4bE0rTzh5WXc9PSIsInZhbHVlIjoiRU10MGw1R2JLRHAxVmRBYnRiRjdXVDBUUTRjV2lrWHk1MHFZMG9yQWtSYz0iLCJtYWMiOiI5MWFiODNlZTI0NzE5YWZlYzZjNWNjYjFhMzgwNGQxNWIxMmFlYzQxNmNhOTdjYTQ3YzI3MDg2MGMzNGVlMDQ4In0',
    'callback_url' => 'https://webhook.site/fec812d6-8377-4313-9c77-ac7e3896c71e'
];
