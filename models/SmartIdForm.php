<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * SmartIdForm is the model behind the contact form.
 */
class SmartIdForm extends Model
{
    public $name;
    public $last_name;
    public $full_name;
    public $rut;
    public $serial_number;
    public $ci_expiration;
    public $age;
    public $gender;
    public $callback_url;


    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['callback_url'],'string'],
            [['name','last_name','full_name','rut','serial_number','ci_expiration','age','gender','callback_url'], 'safe'],
            [['name','last_name','full_name','rut','serial_number','ci_expiration','age','gender'], 'boolean'],
        ];
    }

    /**
     * @return array customized attribute labels
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Nombre',
            'last_name' => 'Apellido',
            'full_name' => 'Nombre Completo',
            'rut' => 'Rut',
            'serial_number' => 'Numero de Serie',
            'ci_expiration' => 'Fecha de Expiracion',
            'age' => 'Edad',
            'gender' => 'Genero',
            'callback_url' => 'URL de Retorno',
        ];
    }

}
